import React from "react";
import { Card } from "react-bootstrap";

const CardComponent = ({ url, title }) => {
  return (
    <Card style={{ height: "100%" }}>
      <Card.Img variant="top" src={url} />
      <Card.Body>
        <Card.Title>{title}</Card.Title>
      </Card.Body>
    </Card>
  );
};

export default CardComponent;
