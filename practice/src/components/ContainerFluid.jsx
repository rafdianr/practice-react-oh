import React from "react";
import { Container } from "react-bootstrap";

const ContainerFluid = (props) => {
  return <Container fluid="md">{props.children}</Container>;
};

export default ContainerFluid;
