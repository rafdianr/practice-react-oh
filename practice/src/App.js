import React from "react";
import MainPage from "./pages/MainPage";
import configureStore from "./store/configure-store";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import "./assets/styles/styles.css";

const { store, persistor } = configureStore();
const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <MainPage />
      </PersistGate>
    </Provider>
  );
};

export default App;
