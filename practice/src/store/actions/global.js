export const getAllData = (payload) => {
  return {
    type: "GET_DATA",
    payload,
  };
};

export const changePaginate = (payload) => {
  return {
    type: "CHANGE_PAGINATE",
    payload,
  };
};
