const initialState = {
  data: [],
  currentPaginate: 1,
};

const global = (state = initialState, action) => {
  switch (action.type) {
    case "GET_DATA":
      return {
        ...state,
        data: action.payload,
      };
    case "CHANGE_PAGINATE":
      return {
        ...state,
        currentPaginate: action.payload,
      };
    default:
      return state;
  }
};

export default global;
