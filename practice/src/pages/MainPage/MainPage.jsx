import React from "react";
import Header from "./Header";
import CarouselContent from "./CarouselContent";
import CardList from "./CardList";

const MainPage = () => {
  return (
    <div className="pageScreen" id="mainPage">
      <Header />
      <CarouselContent />
      <CardList />
    </div>
  );
};

export default MainPage;
