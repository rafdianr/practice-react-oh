import React from "react";
import ContainerFluid from "../../components/ContainerFluid";
import { Carousel } from "react-bootstrap";
import Images1 from "../../assets/images/carousel1.jpeg";
import Images2 from "../../assets/images/carousel2.jpeg";
import Images3 from "../../assets/images/carousel3.jpeg";

const CarouselContent = () => {
  const carouselStyle = {
    height: "60vh",
    width: "100%",
  };
  const carouselImagesStyle = {
    borderRadius: "5px",
    height: "100%",
    width: "100%",
    objectFit: "cover",
  };
  const carouselItems = [
    { images: Images1 },
    { images: Images2 },
    { images: Images3 },
  ];
  return (
    <div className="carouselContent" style={{ padding: "20px 0" }}>
      <ContainerFluid>
        <Carousel fade controls={false}>
          {carouselItems.map((item, index) => (
            <Carousel.Item key={index} style={carouselStyle}>
              <img
                className="d-block w-100"
                src={item.images}
                alt="First slide"
                style={carouselImagesStyle}
              />
            </Carousel.Item>
          ))}
        </Carousel>
      </ContainerFluid>
    </div>
  );
};

export default CarouselContent;
