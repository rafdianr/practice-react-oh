import React, { useEffect } from "react";
import ContainerFluid from "../../components/ContainerFluid";
import { Row, Col, Pagination } from "react-bootstrap";
import axios from "axios";
import CardComponent from "../../components/CardComponent";
import { useDispatch, useSelector } from "react-redux";
import { changePaginate, getAllData } from "../../store/actions/global";

const CardList = () => {
  const cards = useSelector((state) => state.global.data);
  const currentPaginate = useSelector((state) => state.global.currentPaginate);
  const dispatch = useDispatch();
  const maxData = 10;

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/albums/1/photos")
      .then((res) => dispatch(getAllData(res.data)))
      .catch((e) => console.log({ e }));
  }, [dispatch]);

  const paginateClick = (num) => {
    dispatch(changePaginate(num));
  };

  const PaginateItem = () => {
    if (!cards.length) return null;

    let items = [];
    let maxItems = Math.ceil(cards.length / maxData);
    for (let i = 1; i <= maxItems; i++) {
      items.push(
        <Pagination.Item
          key={i}
          active={i === currentPaginate}
          onClick={() => paginateClick(i)}
        >
          {i}
        </Pagination.Item>
      );
    }
    return items;
  };

  return (
    <div className="cardList" style={{ padding: "10px 0" }}>
      <ContainerFluid>
        <h4 className="cardList__title">List of Cards</h4>

        <Pagination size="sm" style={{ justifyContent: "center" }}>
          <PaginateItem />
        </Pagination>

        <div className="cardList__item">
          <Row xs={1} sm={2} md={5} className="g-4">
            {cards
              .slice((currentPaginate - 1) * maxData, currentPaginate * maxData)
              .map((item, idx) => (
                <Col key={idx}>
                  <CardComponent url={item.url} title={item.title} />
                </Col>
              ))}
          </Row>
        </div>
      </ContainerFluid>
    </div>
  );
};

export default CardList;
