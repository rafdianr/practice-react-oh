import React, { useState } from "react";
import { Navbar, Nav } from "react-bootstrap";
import ContainerFluid from "../../components/ContainerFluid";
import img from "../../assets/images/carousel1.jpeg";

const Header = () => {
  const [isLogin, setIslogin] = useState(false);

  const teestAvatar = {
    width: "50px",
    height: "50px",
    backgroundImage: `url(${img})`,
    borderRadius: "50%",
    border: "1px solid #ffffff",
  };
  return (
    <div className="header">
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <ContainerFluid>
          <Navbar.Brand
            href="#home"
            style={{ fontWeight: "700" }}
            className="custom-navbar-brand"
          >
            Rafd
            {/* {isLogin ? <div style={teestAvatar}></div> : <button>Login</button>} */}
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="#features">Test</Nav.Link>
              <Nav.Link href="#pricing">Lists</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </ContainerFluid>
      </Navbar>
    </div>
  );
};

export default Header;
